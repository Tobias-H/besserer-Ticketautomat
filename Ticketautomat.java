/**
 * Die Klasse Ticketautomat modelliert einfache Ticketautomaten,
 * die Tickets zu einem Einheitspreis herausgeben.
 * Der Preis f�r die Tickets eines Automaten kann �ber den Konstruktor
 * festgelegt werden.
 * Ticketautomaten dieser Klasse pr�fen, ob sinnvolle Geldbetr�ge
 * eingeworfen werden, und drucken ein Ticket nur dann, wenn
 * ausreichend Geld eingeworfen wurde.
 *
 * @author David J. Barnes und Michael K�lling
 * @version 2016.02.29
 */
public class Ticketautomat
{
    // Der Preis eines Tickets dieses Automaten.
    private int preis;
    // Der Betrag, der bisher vom Automatenbenutzer eingeworfen wurde.
    private int bisherGezahlt;
    // Die Geldsumme, die bisher von diesem Automaten eingenommen wurde.
    private int gesamtsumme;
    // Der Rabatt eines Tickets dieses Automaten.
    private double rabatt;
    // Der Betrag, den man mit einem Rabatt spart.
    private double ersparnis;

    /**
     * Erzeuge einen Automaten, der Tickets zum angegebenen
     * Preis ausgibt.
     */
    public Ticketautomat(int ticketpreis)
    {
        preis = ticketpreis;
        bisherGezahlt = 0;
        gesamtsumme = 0;
        rabatt = 0;
        ersparnis = 0;
    }

    /**
     * Liefere den Preis eines Tickets dieses Automaten.
     */
    public int gibPreis()
    {
        return preis;
    }

    /**
     * Liefere die H�he des Betrags, der f�r das n�chste Ticket bereits
     * eingeworfen wurde.
     */
    public int gibBisherGezahltenBetrag()
    {
        return bisherGezahlt;
    }

    /**
     * Nimm den angegebenen Betrag als Anzahlung f�r das 
     * n�chste Ticket. Pr�fe, ob der Betrag sinnvoll ist.
     */
    public void geldEinwerfen(int betrag)
    {
        if (betrag < 0) {
            System.out.println("Bitte nur positive Betr�ge verwenden: "
                                + betrag);
        }
        else {
            bisherGezahlt = bisherGezahlt + betrag;
        }
    }

    /**
     * Drucke ein Ticket, wenn gen�gend Geld eingeworfen wurde, und 
     * ziehe den Ticketpreis vom bisher gezahlten Betrag ab. Gib eine 
     * Fehlermeldung aus, falls noch Geld f�r ein Ticket fehlt.
     */
    public void ticketDrucken()
    {
        int nochZuZahlen = preis - bisherGezahlt;
        if (nochZuZahlen <= 0) {
            // Den Ausdruck eines Tickets simulieren.
            System.out.println("##################");
            System.out.println("# Die BlueJ-Linie");
            System.out.println("# Ticket");
            System.out.println("# " + preis + " Cent.");
            System.out.println("##################");
            System.out.println();
    
            // Die Gesamtsumme um den Ticketpreis erh�hen.
            gesamtsumme = gesamtsumme + preis;
            // Den Preis von der bisherigen Bezahlung abziehen.
            bisherGezahlt = bisherGezahlt - preis;
        }
        else {
            System.out.println("Sie m�ssen noch mindestens " +
                               (nochZuZahlen) +
                               " Cent einwerfen.");
        }
    }
    
    /**
     * Gib das Wechselgeld bzw. den bisher gezahlten Betrag zur�ck.
     * Setze den bisher gezahlten Betrag zur�ck auf 0.
     */
    public int wechselgeldAuszahlen()
    {
        int wechselgeld;
        wechselgeld = bisherGezahlt;
        bisherGezahlt = 0;
        return wechselgeld;
    }
    
    /**
     * Berechnet das Ersparnis mit einem bestimmten Rabatt.
     */
    public double gibErsparnis(){
        return preis * rabatt;
    }
    
    /**
     * Untersucht, ob schon genug gezahlt wurde.
     */
    public void istBudgetGenug(){
        if(preis > bisherGezahlt){
            System.out.println("zu teuer, ihr aktuelles Budget ligt bei " + bisherGezahlt + "ct");
        } else {
            System.out.println("passt");
        }
    }
    
    /**
     * Entleert den Automaten
     */
    public int entleeren(){
        int r�ckgeld = gesamtsumme;
        gesamtsumme = 0;
        return r�ckgeld;
    }
}
